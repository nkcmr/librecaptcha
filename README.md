# librecaptcha

librecaptcha renders and verifies [reCAPTCHA](http://www.google.com/recaptcha) captchas.

## Installation

Via npm:
```
$ npm install librecaptcha
```

Via git:
```
$ git clone https://github.com/nkcmr/librecaptcha.git
```

## Setup

Before you can use this module, you must visit http://www.google.com/recaptcha
to request a public and private API key for your domain.

This package has no external dependencies.

## Usage

To get started, use `require` to load `recaptcha` in your script.
```js
var reCAPTCHA = require("librecaptcha");
```

Next, initialize a recaptcha singleton that will be used throughout your script.
```js
var captcha = new reCAPTCHA({
    public_key: "pub_key",
    private_key: "private_key"
});
```
Cool! Now you are ready to generate form snippets and verify submissions!

## API
#### reCAPTCHA
Exposed by `require("librecaptcha")`

#### reCAPTCHA(config:Object)
Create a new instance of reCAPTCHA. Available options are as follows:

 - `public_key` The public key provided by the reCAPTCHA website (see Setup)
 - `private_key` The private key provided by the reCAPTCHA website (see Setup) 

#### captcha.generate()
Renders and returns a reCAPTCHA snippet as a string.

#### captcha.verify(recaptcha_data:Object, callback:Function)
Takes in and sends verification request to reCAPTCHA API. Then parses response from server then calls `callback`. If verification fails, `callback` will be called with the reason for failure as the first parameter.

The `recaptcha_data` object contains the following items:

 - `remoteip` The IP address of the client submitting a reCAPTCHA
 - `challenge` The parameter sent by the client-side form, it should be sent by the form as `recaptcha_challenge_field`
 - `response` The parameter sent by the client-side form, it should be sent by the form as `recaptcha_response_field`

**Example**:
```js
var recaptcha_data = {
    remoteip: "74.125.131.113",
    challenge: req.body.recaptcha_challenge_field,
    response: req.body.recaptcha_response_field
};

captcha.verify(recaptcha_data, function(err){
    if(err) {
        // Error Code Reference - https://developers.google.com/recaptcha/docs/verify
        console.error(err);
        if(err == "incorrect-captcha-sol") {
            res.send("you failed!!");
        }
        return;
    }
    // If no error, continue! User has correctly entered the captcha
    res.send("w00t!");
});
```


## Example Using [Express](http://www.expressjs.com)

app.js:

```js
var express = require('express');
var reCAPTCHA = require('librecaptcha');
var http = require("http");

var PUBLIC_KEY  = 'public_key';
var PRIVATE_KEY = 'private_key';

var captcha = new reCAPTCHA({
    public_key: PUBLIC_KEY,
    private_key: PRIVATE_KEY
});

var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.bodyParser());

// Routes
app.get('/', function(req, res) {
    res.render('form', {
        recaptcha_form: captcha.generate()
    });
});

app.post('/', function(req, res) {
    var recaptcha_data = {
            remoteip:  req.ip,
            challenge: req.body.recaptcha_challenge_field,
            response:  req.body.recaptcha_response_field
    };

    captcha.verify(recaptcha_data, function(err) {
        if(err) {
            return res.render('form', {
                recaptcha_form: captcha.toHTML()
            });
        }

        console.log("captcha was entered correctly");
        res.send("yay!");
    });
});

http.createServer(app).listen(8080);
```


views/form.jade:

```jade
form(method='POST', action='/')
  != recaptcha_form
  input(type='submit', value='Check Recaptcha')
```

Make sure [express](http://www.expressjs.com) and [jade](http://jade-lang.com)
are installed, then:

    $ node app.js

## The MIT License (MIT)
Copyright (c) 2013 Nick Comer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
