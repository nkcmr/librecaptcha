var Recaptcha = require(__dirname + '/lib/recaptcha'),
    http = require('http'),
    events = require('events'),
    util = require('util'),
    querystring = require('querystring');

exports['Recaptcha construction'] = function(test) {
    var recaptcha = new Recaptcha({
        public_key: "PUBLIC",
        private_key: "PRIVATE"
    });

    test.strictEqual(recaptcha.config.public_key, 'PUBLIC', 'public_key is set');
    test.strictEqual(recaptcha.config.private_key, 'PRIVATE', 'private_key is set');
    test.done();
};

exports['Recaptcha client-side form generation'] = function(test) {
    var recaptcha = new Recaptcha({
        public_key: "PUBLIC",
        private_key: "PRIVATE"
    });

    var script_src = '//www.google.com/recaptcha/api/challenge?k=PUBLIC';
    var noscript_src = '//www.google.com/recaptcha/api/noscript?k=PUBLIC';

    var expected_html = util.format('<script type="text/javascript" src="%s"></script><noscript><iframe src="%s" height="300" width="500" frameborder="0"></iframe><br><textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea><input type="hidden" name="recaptcha_response_field" value="manual_challenge"></noscript>', script_src, noscript_src);

    test.strictEqual(recaptcha.generate(), expected_html, 'toHTML() returns expected HTML');
    test.done();
};

exports['verify() with no data'] = function(test) {
    var recaptcha = new Recaptcha({
        public_key: "PUBLIC",
        private_key: "PRIVATE"
    });

    var create_client_called = false;

    // We shouldn't need to contact Recaptcha to know this is invalid.
    http.request = function(options, callback) {
        create_client_called = true;
    };

    recaptcha.verify({}, function(err) {
        test.strictEqual(err, 'verify-params-incorrect');
        // Ensure that http.request() was never called.
        test.strictEqual(create_client_called, false);

        test.done();
    });
};

exports['verify() with blank response'] = function(test) {
    var data = {
        remoteip: '127.0.0.1',
        challenge: 'challenge',
        response: ''
    };
    var recaptcha = new Recaptcha({
        public_key: "PUBLIC",
        private_key: "PRIVATE"
    });

    var create_client_called = false;

    http.request = function(options, callback) {
        create_client_called = true;
    };

    recaptcha.verify(data, function(err) {
        test.strictEqual(err, 'incorrect-captcha-sol');
        test.strictEqual(create_client_called, false);
        test.done();
    });
};

exports['verify() with blank remoteip'] = function(test) {
    var data = {
        challenge: 'challenge',
        response: 'response',
        remoteip: ''
    };
    var recaptcha = new Recaptcha({
        public_key: "PUBLIC",
        private_key: "PRIVATE"
    });

    var create_client_called = false;

    // We shouldn't need to contact Recaptcha to know this is invalid.
    http.request = function(options, callback) {
        create_client_called = true;
    };

    recaptcha.verify(data, function(err) {
        test.strictEqual(err, 'verify-params-incorrect');
        test.strictEqual(create_client_called, false);
        test.done();
    });
};

exports['verify() with blank challenge'] = function(test) {
    var data = {
        remoteip: '127.0.0.1',
        response: 'response',
        challenge: ''
    };
    var recaptcha = new Recaptcha({
        public_key: "PUBLIC",
        private_key: "PRIVATE"
    });

    var create_client_called = false;

    // We shouldn't need to contact Recaptcha to know this is invalid.
    http.request = function(options, callback) {
        create_client_called = true;
    };

    recaptcha.verify(data, function(err) {
        test.strictEqual(err, 'verify-params-incorrect');
        test.strictEqual(create_client_called, false);

        test.done();
    });
};