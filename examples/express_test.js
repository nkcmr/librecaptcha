var express = require('express');
var	Recaptcha = require('../lib/recaptcha');
var	http = require("http");

var PUBLIC_KEY  = 'public_key';
var	PRIVATE_KEY = 'private_key';

var recaptcha = new Recaptcha({
	public_key: PUBLIC_KEY,
	private_key: PRIVATE_KEY
});

var app = express();

app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.bodyParser());

// Routes
app.get('/', function(req, res) {
	res.render('form', {
		recaptcha_form: recaptcha.generate()
	});
});

app.post('/', function(req, res) {
	var recaptcha_data = {
			remoteip:  req.ip,
			challenge: req.body.recaptcha_challenge_field,
			response:  req.body.recaptcha_response_field
	};

	recaptcha.verify(recaptcha_data, function(err) {
		if(err) {
			return res.render('form', {
				recaptcha_form: recaptcha.generate()
			});
		}

		console.log("captcha was entered correctly");
		res.send("yay!");
	});
});

http.createServer(app).listen(8080);