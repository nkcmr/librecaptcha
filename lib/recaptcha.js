/**
 * librecaptcha
 * Copyright(c) 2013 Nick Comer <nick@comer.io>
 * MIT Licensed
 */

var http = require('http');
var util = require('util');
var querystring = require('querystring');

var api_host      = 'www.google.com',
    api_end_point = '/recaptcha/api/verify',
    script_src    = api_host + '/recaptcha/api/challenge',
    noscript_src  = api_host + '/recaptcha/api/noscript';

/**
 * Initialize a new reCAPTCHA module
 * @param {Object} config An object that contains a reCAPTCHA public_key and private_key
 * @return {null|Object} If the parameters passed to reCAPTCHA were correct it will return a new reCAPTCHA object, else it will throw an error.
 * 
 * @api public
 */
function reCAPTCHA(config) {
    this.config = config;

    if(this.config.hasOwnProperty("public_key") && this.config.hasOwnProperty("private_key")) {
        return this;
    } else {
        throw new Error("Public and Private key must be supplied to constructor!");
    }
}

/**
 * Render the reCAPTCHA
 * @return {String} An HTML snippet that can be passed to a templating engine or something.
 */
reCAPTCHA.prototype.generate = function() {
    var query_string = util.format("?k=%s", this.config.public_key);

    var a = util.format("//%s%s", script_src, query_string);
    var b = util.format("//%s%s", noscript_src, query_string);

    return util.format("<script type=\"text/javascript\" src=\"%s\"></script><noscript><iframe src=\"%s\" height=\"300\" width=\"500\" frameborder=\"0\"></iframe><br><textarea name=\"recaptcha_challenge_field\" rows=\"3\" cols=\"40\"></textarea><input type=\"hidden\" name=\"recaptcha_response_field\" value=\"manual_challenge\"></noscript>", a, b);
};

/**
 * Verify a reCAPTCHA submission
 * @param  {Object}   recaptcha_data An object containing the following keys: remoteip (IP Address of the submitter), challenge, and response
 * @param  {Function} callback       A function that will be called once everything is done
 *
 * @api public
 */
reCAPTCHA.prototype.verify = function(recaptcha_data, callback) {
    var self = this;

    if(typeof callback === "function") {
        if(recaptcha_data.hasOwnProperty("remoteip") && recaptcha_data.hasOwnProperty("challenge") && recaptcha_data.hasOwnProperty("response")) {

            if(recaptcha_data.response == "") {
                callback("incorrect-captcha-sol");
                return;
            }

            if(recaptcha_data.challenge == "") {
                callback("verify-params-incorrect");
                return;
            }

            if(recaptcha_data.remoteip == "") {
                callback("verify-params-incorrect");
                return;
            }

            // All parameters check out -- lets continue
            var post_data = recaptcha_data;
            post_data.privatekey = this.config.private_key;
            post_data = querystring.stringify(post_data);

            var req_options = {
                host: api_host,
                path: api_end_point,
                port: 80,
                method: "POST",
                headers: {
                    "Content-Type": "application/x-www-form-urlencoded",
                    "Content-Length": post_data.length
                }
            };

            var request = http.request(req_options, function(response){

                var body = "";

                response.on("error", function(err){
                    callback("recaptcha-not-reachable");
                });

                response.on("data", function(chunk){
                    body += chunk;
                });

                response.on("end", function(){
                    var parts = body.split("\n");
                    var status = (parts[0] === "true");
                    if(status) {
                        callback(null);
                    } else {
                        callback(parts[1]);
                    }
                });
            });
            request.write(post_data, "utf8");
            request.end();

        } else {
            callback("verify-params-incorrect");
        }
    } else {
        throw new Error("Second parameter must be a function")
    }
};

module.exports = reCAPTCHA;